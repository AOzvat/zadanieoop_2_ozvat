#ifndef CALCULATOR_H
#define CALCULATOR_H

#include <QtWidgets/QMainWindow>
#include "ui_calculator.h"
#include <QLabel>
#include<QtCore/qmath.h>

class Calculator : public QMainWindow
{
	Q_OBJECT

public:
	Calculator(QWidget *parent = 0);
	~Calculator();

	public slots:
		void vymaz();
		void rovna_sa();
		void plus();
		void minus();
		void nasobenie();
		void delenie();
		void odmocnina();
		void mocnina();
		void znamienko();
		void bodka();
		void jeden();
		void dva();
		void tri();
		void stiri();
		void pat();
		void sest();
		void sedem();
		void osem();
		void devat();
		void nula();

private:
	Ui::CalculatorClass ui;

	QString hodnota = "", sucet = "";
	float fNum, sNum;
	bool plusBool = false, minusBool = false, nasobBool = false, delBool = false, odmBool = false, mocBool = false, znamBool = false, bodBool = false;
};

#endif // CALCULATOR_H

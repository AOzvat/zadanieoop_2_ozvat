/********************************************************************************
** Form generated from reading UI file 'calculator.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CALCULATOR_H
#define UI_CALCULATOR_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CalculatorClass
{
public:
    QWidget *centralWidget;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QPushButton *pushButton_3;
    QPushButton *pushButton_4;
    QPushButton *pushButton_5;
    QPushButton *pushButton_6;
    QPushButton *pushButton_7;
    QPushButton *pushButton_8;
    QPushButton *pushButton_9;
    QPushButton *pushButton_10;
    QPushButton *pushButton_11;
    QPushButton *pushButton_12;
    QPushButton *pushButton_13;
    QPushButton *pushButton_14;
    QPushButton *pushButton_15;
    QPushButton *pushButton_16;
    QPushButton *pushButton_17;
    QPushButton *pushButton_18;
    QPushButton *pushButton_19;
    QPushButton *pushButton_20;
    QLabel *label;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *CalculatorClass)
    {
        if (CalculatorClass->objectName().isEmpty())
            CalculatorClass->setObjectName(QStringLiteral("CalculatorClass"));
        CalculatorClass->resize(293, 329);
        centralWidget = new QWidget(CalculatorClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(20, 60, 51, 41));
        pushButton_2 = new QPushButton(centralWidget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(20, 100, 51, 41));
        pushButton_3 = new QPushButton(centralWidget);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));
        pushButton_3->setGeometry(QRect(20, 140, 51, 41));
        pushButton_4 = new QPushButton(centralWidget);
        pushButton_4->setObjectName(QStringLiteral("pushButton_4"));
        pushButton_4->setGeometry(QRect(20, 180, 51, 41));
        pushButton_5 = new QPushButton(centralWidget);
        pushButton_5->setObjectName(QStringLiteral("pushButton_5"));
        pushButton_5->setGeometry(QRect(70, 60, 51, 41));
        pushButton_6 = new QPushButton(centralWidget);
        pushButton_6->setObjectName(QStringLiteral("pushButton_6"));
        pushButton_6->setGeometry(QRect(70, 100, 51, 41));
        pushButton_7 = new QPushButton(centralWidget);
        pushButton_7->setObjectName(QStringLiteral("pushButton_7"));
        pushButton_7->setGeometry(QRect(70, 140, 51, 41));
        pushButton_8 = new QPushButton(centralWidget);
        pushButton_8->setObjectName(QStringLiteral("pushButton_8"));
        pushButton_8->setGeometry(QRect(70, 180, 51, 41));
        pushButton_9 = new QPushButton(centralWidget);
        pushButton_9->setObjectName(QStringLiteral("pushButton_9"));
        pushButton_9->setGeometry(QRect(120, 60, 51, 41));
        pushButton_10 = new QPushButton(centralWidget);
        pushButton_10->setObjectName(QStringLiteral("pushButton_10"));
        pushButton_10->setGeometry(QRect(120, 100, 51, 41));
        pushButton_11 = new QPushButton(centralWidget);
        pushButton_11->setObjectName(QStringLiteral("pushButton_11"));
        pushButton_11->setGeometry(QRect(120, 140, 51, 41));
        pushButton_12 = new QPushButton(centralWidget);
        pushButton_12->setObjectName(QStringLiteral("pushButton_12"));
        pushButton_12->setGeometry(QRect(120, 180, 51, 41));
        pushButton_13 = new QPushButton(centralWidget);
        pushButton_13->setObjectName(QStringLiteral("pushButton_13"));
        pushButton_13->setGeometry(QRect(20, 220, 151, 41));
        pushButton_14 = new QPushButton(centralWidget);
        pushButton_14->setObjectName(QStringLiteral("pushButton_14"));
        pushButton_14->setGeometry(QRect(170, 220, 51, 41));
        pushButton_15 = new QPushButton(centralWidget);
        pushButton_15->setObjectName(QStringLiteral("pushButton_15"));
        pushButton_15->setGeometry(QRect(170, 180, 51, 41));
        pushButton_16 = new QPushButton(centralWidget);
        pushButton_16->setObjectName(QStringLiteral("pushButton_16"));
        pushButton_16->setGeometry(QRect(170, 140, 51, 41));
        pushButton_17 = new QPushButton(centralWidget);
        pushButton_17->setObjectName(QStringLiteral("pushButton_17"));
        pushButton_17->setGeometry(QRect(170, 100, 51, 41));
        pushButton_18 = new QPushButton(centralWidget);
        pushButton_18->setObjectName(QStringLiteral("pushButton_18"));
        pushButton_18->setGeometry(QRect(220, 180, 51, 81));
        pushButton_19 = new QPushButton(centralWidget);
        pushButton_19->setObjectName(QStringLiteral("pushButton_19"));
        pushButton_19->setGeometry(QRect(220, 100, 51, 81));
        pushButton_20 = new QPushButton(centralWidget);
        pushButton_20->setObjectName(QStringLiteral("pushButton_20"));
        pushButton_20->setGeometry(QRect(170, 60, 101, 41));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(20, 20, 251, 31));
        QFont font;
        font.setPointSize(18);
        label->setFont(font);
        label->setMargin(6);
        CalculatorClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(CalculatorClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 293, 21));
        CalculatorClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(CalculatorClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        CalculatorClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(CalculatorClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        CalculatorClass->setStatusBar(statusBar);

        retranslateUi(CalculatorClass);
        QObject::connect(pushButton_4, SIGNAL(clicked()), CalculatorClass, SLOT(jeden()));
        QObject::connect(pushButton_8, SIGNAL(clicked()), CalculatorClass, SLOT(dva()));
        QObject::connect(pushButton_12, SIGNAL(clicked()), CalculatorClass, SLOT(tri()));
        QObject::connect(pushButton_13, SIGNAL(clicked()), CalculatorClass, SLOT(nula()));
        QObject::connect(pushButton_3, SIGNAL(clicked()), CalculatorClass, SLOT(stiri()));
        QObject::connect(pushButton_7, SIGNAL(clicked()), CalculatorClass, SLOT(pat()));
        QObject::connect(pushButton_11, SIGNAL(clicked()), CalculatorClass, SLOT(sest()));
        QObject::connect(pushButton_2, SIGNAL(clicked()), CalculatorClass, SLOT(sedem()));
        QObject::connect(pushButton_6, SIGNAL(clicked()), CalculatorClass, SLOT(osem()));
        QObject::connect(pushButton_10, SIGNAL(clicked()), CalculatorClass, SLOT(devat()));
        QObject::connect(pushButton_20, SIGNAL(clicked()), CalculatorClass, SLOT(vymaz()));
        QObject::connect(pushButton_19, SIGNAL(clicked()), CalculatorClass, SLOT(plus()));
        QObject::connect(pushButton_17, SIGNAL(clicked()), CalculatorClass, SLOT(delenie()));
        QObject::connect(pushButton_16, SIGNAL(clicked()), CalculatorClass, SLOT(nasobenie()));
        QObject::connect(pushButton_15, SIGNAL(clicked()), CalculatorClass, SLOT(minus()));
        QObject::connect(pushButton_18, SIGNAL(clicked()), CalculatorClass, SLOT(rovna_sa()));
        QObject::connect(pushButton_9, SIGNAL(clicked()), CalculatorClass, SLOT(odmocnina()));
        QObject::connect(pushButton_5, SIGNAL(clicked()), CalculatorClass, SLOT(mocnina()));
        QObject::connect(pushButton, SIGNAL(clicked()), CalculatorClass, SLOT(znamienko()));
        QObject::connect(pushButton_14, SIGNAL(clicked()), CalculatorClass, SLOT(bodka()));

        QMetaObject::connectSlotsByName(CalculatorClass);
    } // setupUi

    void retranslateUi(QMainWindow *CalculatorClass)
    {
        CalculatorClass->setWindowTitle(QApplication::translate("CalculatorClass", "Calculator", 0));
        pushButton->setText(QApplication::translate("CalculatorClass", "+/-", 0));
        pushButton_2->setText(QApplication::translate("CalculatorClass", "7", 0));
        pushButton_3->setText(QApplication::translate("CalculatorClass", "4", 0));
        pushButton_4->setText(QApplication::translate("CalculatorClass", "1", 0));
        pushButton_5->setText(QApplication::translate("CalculatorClass", "x\302\262", 0));
        pushButton_6->setText(QApplication::translate("CalculatorClass", "8", 0));
        pushButton_7->setText(QApplication::translate("CalculatorClass", "5", 0));
        pushButton_8->setText(QApplication::translate("CalculatorClass", "2", 0));
        pushButton_9->setText(QApplication::translate("CalculatorClass", "\342\210\232x", 0));
        pushButton_10->setText(QApplication::translate("CalculatorClass", "9", 0));
        pushButton_11->setText(QApplication::translate("CalculatorClass", "6", 0));
        pushButton_12->setText(QApplication::translate("CalculatorClass", "3", 0));
        pushButton_13->setText(QApplication::translate("CalculatorClass", "0", 0));
        pushButton_14->setText(QApplication::translate("CalculatorClass", ".", 0));
        pushButton_15->setText(QApplication::translate("CalculatorClass", "-", 0));
        pushButton_16->setText(QApplication::translate("CalculatorClass", "*", 0));
        pushButton_17->setText(QApplication::translate("CalculatorClass", "/", 0));
        pushButton_18->setText(QApplication::translate("CalculatorClass", "=", 0));
        pushButton_19->setText(QApplication::translate("CalculatorClass", "+", 0));
        pushButton_20->setText(QApplication::translate("CalculatorClass", "C", 0));
        label->setText(QApplication::translate("CalculatorClass", "0", 0));
    } // retranslateUi

};

namespace Ui {
    class CalculatorClass: public Ui_CalculatorClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CALCULATOR_H

#include "calculator.h"

Calculator::Calculator(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
}

void Calculator::vymaz() {
	hodnota = "";
	sucet = "";
	fNum = 0;
	sNum = 0;
	plusBool = false; 
	minusBool = false;
	nasobBool = false;
	delBool = false;
	odmBool = false;
	mocBool = false;
	znamBool = false;
	ui.label->setText(hodnota);
}

void Calculator::rovna_sa() {
	sNum = hodnota.toFloat();
	if (plusBool) {
		sucet = QString::number(fNum + sNum);
		hodnota = sucet;
		ui.label->setText(sucet);
		plusBool = false;
	}
	if (minusBool) {
		sucet = QString::number(fNum - sNum);
		hodnota = sucet;
		ui.label->setText(sucet);
		minusBool = false;
	}
	if (nasobBool) {
		sucet = QString::number(fNum * sNum);
		hodnota = sucet;
		ui.label->setText(sucet);
		nasobBool = false;
	}
	if (delBool) {
		sucet = QString::number(fNum / sNum);
		hodnota = sucet;
		ui.label->setText(sucet);
		delBool = false;
	}
	if (odmBool) {
		sucet = QString::number(qSqrt(fNum));
		hodnota = sucet;
		ui.label->setText(sucet);
		odmBool = false;
	}
	if (mocBool) {
		sucet = QString::number(qPow(fNum,2));
		hodnota = sucet;
		ui.label->setText(sucet);
		mocBool = false;
	}
	if (znamBool) {
		sucet = QString::number(-fNum);
		hodnota = sucet;
		ui.label->setText(sucet);
		znamBool = false;
	}
}

void Calculator::plus() {
	fNum = hodnota.toFloat();
	hodnota = "";
	ui.label->setText(hodnota);
	plusBool = true;
}

void Calculator::minus() {
	fNum = hodnota.toFloat();
	hodnota = "";
	ui.label->setText(hodnota);
	minusBool = true;
}

void Calculator::nasobenie() {
	fNum = hodnota.toFloat();
	hodnota = "";
	ui.label->setText(hodnota);
	nasobBool = true;
}

void Calculator::delenie() {
	fNum = hodnota.toFloat();
	hodnota = "";
	ui.label->setText(hodnota);
	delBool = true;
}

void Calculator::odmocnina() {
	fNum = hodnota.toFloat();
	hodnota = "";
	ui.label->setText(hodnota);
	odmBool = true;
}

void Calculator::mocnina() {
	fNum = hodnota.toFloat();
	hodnota = "";
	ui.label->setText(hodnota);
	mocBool = true;
}

void Calculator::znamienko() {
	fNum = hodnota.toFloat();
	hodnota = "";
	ui.label->setText(hodnota);
	znamBool = true;

}

void Calculator::bodka() {
	bodBool=hodnota.contains('.');
	if (!bodBool) {
		hodnota = hodnota + ".";
		ui.label->setText(hodnota);
	}
}

void Calculator::jeden() {
	hodnota = hodnota + "1";
	ui.label->setText(hodnota);
}

void Calculator::dva() {
	hodnota = hodnota + "2";
	ui.label->setText(hodnota);
}

void Calculator::tri() {
	hodnota = hodnota + "3";
	ui.label->setText(hodnota);
}

void Calculator::stiri() {
	hodnota = hodnota + "4";
	ui.label->setText(hodnota);
}

void Calculator::pat() {
	hodnota = hodnota + "5";
	ui.label->setText(hodnota);
}

void Calculator::sest() {
	hodnota = hodnota + "6";
	ui.label->setText(hodnota);
}

void Calculator::sedem() {
	hodnota = hodnota + "7";
	ui.label->setText(hodnota);
}

void Calculator::osem() {
	hodnota = hodnota + "8";
	ui.label->setText(hodnota);
}

void Calculator::devat() {
	hodnota = hodnota + "9";
	ui.label->setText(hodnota);
}

void Calculator::nula() {
	hodnota = hodnota + "0";
	ui.label->setText(hodnota);
}

Calculator::~Calculator()
{

}
